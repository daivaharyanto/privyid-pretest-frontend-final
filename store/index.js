import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'

export const plugins = process.browser
  ? [
      createPersistedState({
        storage: {
          getItem: (key) =>
            new SecureLS({
              isCompression: false,
            }).get(key),
          setItem: (key, value) =>
            new SecureLS({
              isCompression: false,
            }).set(key, value),
          removeItem: (key) =>
            new SecureLS({
              isCompression: false,
            }).remove(key),
        },
      }),
    ]
  : []

export const state = () => ({
  token: [],
  user: [],
  uuid: [],
  message: [],
})

export const mutations = {
  myToken: (state, value) =>
    value ? (state.token = value) : (state.token = ''),
  myUser: (state, value) => (value ? (state.user = value) : (state.user = '')),
  myUuid: (state, value) => (value ? (state.uuid = value) : (state.uuid = '')),
  myMessage: (state, value) =>
    value ? (state.message = value) : (state.message = ''),
}
