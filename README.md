# privyid-pretest-frontend

## How To Run This Project

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## About This Project
This project is developed using Nuxt.Js and Bootstrap Vue. Axios and Vuex are also included inside this project as data fetching framework and state management framework.

